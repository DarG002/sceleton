$(function() {
   
    // Remove video in mobile
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('video').remove();
    }

    // Owl Carousel init
    $('.catalog-jacket-carousel').owlCarousel({
        center: true,
        items: 1,
        dots: true,
        loop: true,
        nav: true,
        navText: ['', ''],
        lazyLoad: true
    });

    $('.catalog-termo-carousel').owlCarousel({
        center: true,
        items: 1,
        dots: true,
        loop: true,
        nav: true,
        navText: ['', ''],
        lazyLoad: true
    });

    $('.catalog-gloves-carousel').owlCarousel({
        center: true,
        items: 1,
        dots: true,
        loop: true,
        nav: true,
        navText: ['', ''],
        lazyLoad: true
    });

    $('.catalog-accessories-carousel').owlCarousel({
        center: true,
        items: 1,
        dots: true,
        loop: true,
        nav: true,
        navText: ['', ''],
        lazyLoad: true
    });

    function selectActiveItem(e) {

        var current = e.item.index;
        var active_element = $(e.target).find(".owl-item").eq(current).find("img").data('active-item');

        var select_item = $(e.target).parent().siblings('.items-info').find('.' + active_element);

        $(e.target).parent().siblings('.items-info').children().each(function(key, item) {
            $(item).removeClass('hidden').hide();
            $(item).hide();
        });
        select_item.fadeIn(400);
    }

    $('.img-block').find('.item').click(function() {
        let src = $(this).find('img').attr('src');
        $(this).addClass('active').siblings().removeClass('active');
        $(this).parents('.items-info').siblings('.catalog').find('.owl-item.active').find('img').attr('src', src);
    });

    $('.catalog-jacket-carousel').on('changed.owl.carousel', function(e) {
        selectActiveItem(e);
    });

    $('.catalog-termo-carousel').on('changed.owl.carousel', function(e) {
        selectActiveItem(e);
    });

    $('.catalog-gloves-carousel').on('changed.owl.carousel', function(e) {
        selectActiveItem(e);
    });

    $('.catalog-accessories-carousel').on('changed.owl.carousel', function(e) {
        selectActiveItem(e);
    });

    $('.news-carousel').owlCarousel({
        items: 4,
        slideBy: 'page',
        nav: true,
        navText: ['', '']
    });

    var speed = 300;

    function itemTransition(item, itemSpeed) {
        var itemWidth = item.width();

        item.find('img').clearQueue().stop().transition({
            x: 0,
            y: 0,
            scale: 1
        }, itemSpeed);
        item.prevAll().find('img').clearQueue().stop().transition({
            x: itemWidth / 2 + 'px',
            y: -100,
            scale: .5
        }, itemSpeed);
        item.nextAll().find('img').clearQueue().stop().transition({
            x: -itemWidth / 2 + 'px',
            y: -100,
            scale: .5
        }, itemSpeed);
        item.clearQueue().stop();
    }

    var $itemCarousel = $('.item-carousel');
    $itemCarousel
        .on('initialized.owl.carousel', function() {
            itemTransition($(this).find('.active'), speed);
        })
        .owlCarousel({
            items: 1,
            dots: false,
            nav: true,
            navText: ['', ''],
            touchDrag: false,
            pullDrag: false,
            mouseDrag: false,
            loop: false,
            lazyLoad: true,
            smartSpeed: speed
        });

    $('.owl-next').on('click', function() {
        itemTransition($itemCarousel.find('.active'), speed);
    });

    $('.owl-prev').on('click', function() {
        itemTransition($itemCarousel.find('.active'), speed);
    });

    // Up button
    $('.up').on('click', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 400);
    });

    // Down button
    $('.button-down').on('click', function() {
        $('html, body').animate({
            scrollTop: $(window).height()
        }, 600);
    });

    // Menu scroll button
    $('.menu-btn').on('click', function() {
        $('html, body').animate({
            scrollTop: $('#' + $(this).data('anchor')).offset().top
        }, 600);
    });

    var time = 0;

    appear({
        init: function init() {
            //console.log('dom is ready');
        },
        elements: function elements() {
            // work with all elements with the class "appear"
            return document.getElementsByClassName('appear');
        },
        appear: function appear(el) {
            //console.log('visible', el);
            // Sidedown button
            setTimeout(function() {
                $('.rect').addClass('animated fadeInDown').removeClass('hidden');
                setTimeout(function() {
                    $('.inner-arrow').addClass('animated fadeInDown').removeClass('hidden');
                }, 100);
            }, 800);
        },
        bounds: 200,
        reappear: false
    });

    var $animate = false,
        $top = 90,
        $speed = 3.6;

    //Paralax
    function paralaxXY(itemShift, item) {
        var shift = Math.sin(itemShift / 30) * 30;
        item.find('.dirt-l2').transition({
            x: itemShift / 3,
            y: -Math.abs(shift) / 3
        }, 0);
        item.find('.dirt-l3').transition({
            x: itemShift / 2,
            y: -Math.abs(shift / 2)
        }, 0);
        item.find('.dirt-l4').transition({
            x: itemShift,
            y: -Math.abs(shift)
        }, 0);
    }

    function paralaxX(itemShift, item) {
        var i = 0,
            itemCount = item.find('img').length;

        item.find('img').each(function() {
            var rate = itemCount - i;
            ++i;
            $(this).transition({
                x: itemShift / rate
            }, i);
        });
    }

    var pageWidth = $(window).width() / 2;
    //var currentPos = 0;

    $('#s02').find('[class*="item-"]').on('mousemove', function(e) {
        var itemShift = -((e.pageX - pageWidth) / 20).toFixed(3);

        $(this).find('img').transition({
            perspective: '500px',
            rotate3d: '0,1,0,' + itemShift / 1.4 + 'deg'
        }, 0);

        if ($(this).hasClass('item-1')) {
            itemShift = -Math.abs(itemShift);
        }

        paralaxXY(itemShift, $(this));
    });

    $('#s04').find('[class*="item-"]').on('mousemove', function(e) {
        var itemShift = -((e.pageX - pageWidth) / 20).toFixed(3);

        $(this).find('img').transition({
            perspective: '500px',
            rotate3d: '0,1,0,' + itemShift / 1.4 + 'deg'
        }, 0);

        if ($(this).hasClass('item-1')) {
            itemShift = -Math.abs(itemShift);
        }

        paralaxXY(itemShift, $(this));
    });

    $('#s06').find('[class*="item-"]').on('mousemove', function(e) {
        var itemShift = -((e.pageX - pageWidth) / 20).toFixed(3);

        $(this).find('img').transition({
            perspective: '500px',
            rotate3d: '0,1,0,' + itemShift / 1.4 + 'deg'
        }, 0);

        if ($(this).hasClass('item-1')) {
            itemShift = -Math.abs(itemShift);
        }

        paralaxXY(itemShift, $(this));
    });

    $('#s08').find('[class*="item-"]').on('mousemove', function(e) {
        var itemShift = -((e.pageX - pageWidth) / 20).toFixed(3);

        $(this).find('img').transition({
            perspective: '500px',
            rotate3d: '0,1,0,' + itemShift / 1.4 + 'deg'
        }, 0);

        if ($(this).hasClass('item-1')) {
            itemShift = -Math.abs(itemShift);
        }

        paralaxXY(itemShift, $(this));
    });

});