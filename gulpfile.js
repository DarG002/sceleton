var gulp         = require('gulp'),
    wiredep      = require('wiredep').stream,
    svgSprite    = require('gulp-svg-sprite'),
    useref       = require('gulp-useref'),
    concatCSS    = require('gulp-concat-css'),
    autoprefixer = require('gulp-autoprefixer'),
    rename       = require('gulp-rename'),
    minifyCSS    = require('gulp-minify-css'),
    less         = require('gulp-less'),
    uncss        = require('gulp-uncss'),
    gulpif       = require('gulp-if'),
    uglify       = require('gulp-uglify'),
    minifyHTML   = require('gulp-minify-html'),
    imagemin     = require('gulp-imagemin'),
    jade         = require('gulp-jade'),
    browserSync  = require('browser-sync').create(),
    path         = require('path'),
    clean        = require('gulp-clean');

//bower-wtrdep подключаем библиотеки
gulp.task('bower', function () {
  gulp.src('./app/index.html')
    .pipe(wiredep({
      directory : "app/components"
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('sprites', function(){
    gulp.src('*.svg', {cwd: './sprites/icons'}).pipe(svgSprite({
        shape: {
            spacing: {
                padding: 2
            }
        },
        mode: {
            css: {
                bust: false,
                dest: '.',
                sprite: 'app/img/sprites.svg',
                layout: 'packed',
                render: {
                    css: true,
                    less: {
                        dest: 'dev/less/sprites.less'
                    }
                }
            }
        },
    })).pipe(gulp.dest('.'));
});

//clean
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

//uncss - убираем все что не используеться
gulp.task('un_css', function() {
    return gulp.src('app/css/style.css')
        .pipe(uncss({
            html: ['app/index.html']
        }))
        .pipe(gulp.dest('app/css'));
});

//uncss bootstrap - убираем все что не используеться
gulp.task('un_css_bootstrap', function() {
    return gulp.src('app/css/bootstrap.css')
        .pipe(uncss({
            html: ['app/index.html']
        }))
        .pipe(gulp.dest('app/css'));
});

// Jade компилируем
gulp.task('jade', function(){
  gulp.src(['dev/jade/*.jade', 'dev/jade/pages/**/*.jade', '!dev/jade/_*.jade'])
    .pipe(jade({
        pretty: true
    }))
    .pipe(gulp.dest('app/'))
});

//less компилируем
gulp.task('less', function () {
  gulp.src('dev/less/style.less')
    .pipe(less())
    .pipe(autoprefixer({
        browsers: [
            //
            // Official browser support policy:
            // http://v4-alpha.getbootstrap.com/getting-started/browsers-devices/#supported-browsers
            //
            'Chrome >= 35', // Exact version number here is kinda arbitrary
            // Rather than using Autoprefixer's native "Firefox ESR" version specifier string,
            // we deliberately hardcode the number. This is to avoid unwittingly severely breaking the previous ESR in the event that:
            // (a) we happen to ship a new Bootstrap release soon after the release of a new ESR,
            //     such that folks haven't yet had a reasonable amount of time to upgrade; and
            // (b) the new ESR has unprefixed CSS properties/values whose absence would severely break webpages
            //     (e.g. `box-sizing`, as opposed to `background: linear-gradient(...)`).
            //     Since they've been unprefixed, Autoprefixer will stop prefixing them,
            //     thus causing them to not work in the previous ESR (where the prefixes were required).
            'Firefox >= 38', // Current Firefox Extended Support Release (ESR); https://www.mozilla.org/en-US/firefox/organizations/faq/
            // Note: Edge versions in Autoprefixer & Can I Use refer to the EdgeHTML rendering engine version,
            // NOT the Edge app version shown in Edge's "About" screen.
            // For example, at the time of writing, Edge 20 on an up-to-date system uses EdgeHTML 12.
            // See also https://github.com/Fyrd/caniuse/issues/1928
            'Edge >= 12',
            'Explorer >= 9',
            // Out of leniency, we prefix these 1 version further back than the official policy.
            'iOS >= 8',
            'Safari >= 8',
            // The following remain NOT officially supported, but we're lenient and include their prefixes to avoid severely breaking in them.
            'Android 2.3',
            'Android >= 4',
            'Opera >= 12'
        ]
    }))
    .pipe(gulp.dest('app/css/'))
});

//less_bootstrap компилируем
gulp.task('less_bootstrap', function () {
  gulp.src('app/components/bootstrap/less/bootstrap.less')
    .pipe(less())
    .pipe(gulp.dest('app/css/'))
});

// image compress
gulp.task('compress', function() {
  gulp.src('app/img/**/*')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/img'))
});

// Static server
gulp.task('default', ['jade', 'less', 'less_bootstrap'], function() {

    browserSync.init({
        server: "./app"
    });
    gulp.watch("dev/jade/**/*.jade", ['jade']);
    gulp.watch("dev/less/**/*.less", ['less']);
    //gulp.watch("app/*.html");
    //gulp.watch("app/js/**/*.html");
});

gulp.task('dev', ['default']);

//build
gulp.task('build', ['clean','compress'], function() {
    //var assets = useref.assets();
    var opts = {
            conditionals: true,
            spare:true
        };

    return gulp.src('app')
        //.pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCSS()))
        //.pipe(assets.restore())
        //.pipe(useref())
        .pipe(gulpif('*.html', minifyHTML(opts)))
        .pipe(gulp.dest('dist'));
});